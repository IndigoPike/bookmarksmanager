﻿using BookmarksManager.Infrastructure.Managers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookmarksManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookmarksController : ControllerBase
    {
        private ChromeManager _chromeManager;
        public BookmarksController(ChromeManager chromeManager)
        {
            _chromeManager = chromeManager;
        }

        [HttpPost]
        public async Task<IActionResult> ImportFromChrome(IFormFile bookmarks)
        {
            using var stream = bookmarks.OpenReadStream();
            var folders = await _chromeManager.ImportFromJson(stream);
            // TODO: add to db

            return Ok();
        }
    }
}
