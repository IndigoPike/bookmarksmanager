﻿using System;

namespace BookmarksManager.Models
{
    public class Tag
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
